﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Marker_ShowDataScript : MonoBehaviour {

    public Text number;
    public Text name;
    public Text coordinates;
    public Text rank;

    public MarkerData markerData;

    public GameObject relatedMarker;

    void OnDisable()
    {
        Destroy(gameObject);
    }
}
