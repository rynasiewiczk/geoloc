﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MarkerPanelScript : MonoBehaviour {

    public CameraZoomToObjectScript camZoomScript;

    public Text myName, coordinates, rank;

    public MarkerScript openedMarker;
    public RectTransform prefabTransform;

    void Start()
    {
        camZoomScript = Camera.main.GetComponent<CameraZoomToObjectScript>();
    }

    public void CollectMarker()
    {
        if(openedMarker != null)
        {
            openedMarker.CollectMarkerMethod();
        }
    }

    public void ActionsForClosingThatPanel()
    {
        if(GetDistance.outOfRangeEvent != null)
        {
            GetDistance.outOfRangeEvent -= ActionsForClosingThatPanel;
        }

        camZoomScript.UnzoomFromObject();

        TouchInputController.freewalk = true;

        Destroy(gameObject);
        //this.gameObject.SetActive(false);

        
    }
}
